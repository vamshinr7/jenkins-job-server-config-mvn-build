Jenkins hosted on ec2 instance
host Ansible master in one ec2 instance
host 3 slave nodes on ec2 instance
Jenkins host => useradd devops = passwd= pass, visudo, sshd and install java & jenkins
AnsibleMaster => useradd ansible = passwd= pass, visudo, sshd and install ansible, sshd-keygen, copy to slave nodes (ssh-copy-id username@publicip)
configure 3 slave nodes => useradd ansible = passwd= pass, visudo, sshd

Go jenkins master and add AnsibleMaster as node and ensure node is launched successfully

go to jenkins dash board 
1. install jenkins plugins
2. configure ansible in globle tool configure 
3. create a parameterized job "server configure" =>
=========================================================================================
pipeline{
    agent {
        node "AnsilbeMaster"
    }
    parameters {
        string (name:'IP', description:'Add ip to configure')
    }
        stages{
            stage(code_checkout){
                steps{
                    git branch: 'main', credentialsId: 'gitlabapikey', url: 'https://gitlab.com/srustikgowda/server-config-ansible.git'
                }
            }
            stage(exicute_playbook){
                steps{
                    ansiblePlaybook extras: "-e 'ADD_IP=${params.IP}' -vvv", installation: 'ansible', inventory: 'hosts', playbook: 'maven_config.yml'
                }
            }
        }
}
============================================================================================
4. add 4 slave server nodes in jenkins under same label
5. build server configuration job and ensure console output git java maven installed and server is configured
============================================================================================

create jenkins jobs to build Maven on above configured servers

run this job on ansiblemaster label (which contains all 4 slaves) 
run maven build job and ensure mvn clean install successfull. 
